import logging
from user_class import User

logger = logging.getLogger('test_logger')
users = [User(email=f'user{i}@domain.com', password=f'superpass{i}') for i in range(1, 11)]


def auth_tests():
    # provision users (register)
    logger.info('provision users')
    for i in range(len(users)):
        users[i].register()

    # test registering existing user
    logger.info(f'registering existing user')
    if not users[0].register():
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test registering user with wrong email
    logger.info('registering user with wrong email')
    if not User(email='not_email', password='random_pass').register():
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # login user properly
    logger.info('logging existing user')
    if users[0].login():
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # login user with invalid credentials
    logger.info('logging user with wrong password')
    if not User(email=users[0].email, password='wrong_password').login():
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # verify valid token
    logger.info('verify valid token')
    users[0].login()
    if users[0].verify():
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # verify wrong token of existing user
    logger.info('verify wrong token of existing user')
    if not User(email=users[0].email, password=users[0].password).verify():
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    return True
