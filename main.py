from healthcheck import health_checks
from auth import auth_tests
from users import users_tests
from friendships import friendship_tests
from devices import device_tests
import requests
import logging
from urls import verify

if not verify:
    requests.packages.urllib3.disable_warnings()

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s | %(levelname)s | %(funcName)10s() | %('
           'message)s',
)
logger = logging.getLogger('test_logger')


def main():
    if not health_checks():
        raise Exception('health_checks() failed')
    if not auth_tests():
        raise Exception('auth_tests() failed')
    if not users_tests():
        raise Exception('user_tests() failed')
    if not friendship_tests():
        raise Exception('friendship_tests() failed')
    if not device_tests():
        raise Exception('friendship_tests() failed')


if __name__ == '__main__':
    main()
