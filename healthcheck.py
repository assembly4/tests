from time import sleep
import requests
from urls import *
import logging

logger = logging.getLogger('test_logger')


def health_check(url):
    r = requests.get(url, verify=verify)
    return True if r.status_code == 200 else False


def health_checks():
    hc_urls = [
        urls.get('health_geneva'),
    ]
    status = False

    for app in hc_urls:
        for i in range(20):
            try:
                status = health_check(app)
            except:
                pass
            if status:
                logging.info(f'{app} | healthy')
                break
            logging.info(f'{app} | unhealthy -> sleeping for {i}')
            sleep(i)

        if not status:
            logging.error(f'{app} - unhealthy')
            return False

    return True
