domain = "localhost"
# domain = "assembly4.tk"
verify = False if domain == "localhost" else True

urls = {
    "health_geneva": f"http://geneva.{domain}/api/v1/health",
    "register": f"http://geneva.{domain}/api/v1/register",
    "login": f"http://geneva.{domain}/api/v1/login",
    "verify": f"http://geneva.{domain}/api/v1/verify",
    "users": f"http://geneva.{domain}/api/v1/users",
    "friendships": f"http://geneva.{domain}/api/v1/friendships",
    "devices": f"http://geneva.{domain}/api/v1/devices",
}
