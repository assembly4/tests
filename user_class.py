import json

import requests
from urls import *
import logging

logger = logging.getLogger('test_logger')


class User:
    def __init__(self, email, password):
        self.email = email
        self.password = password
        self.token = ''
        self.devices = []

    def register(self):
        logger.debug(self.email)
        url = urls.get('register')
        headers = {"Content-Type": "application/json"}
        payload = json.dumps({"email": self.email, "password": self.password})
        r = requests.post(url, headers=headers, data=payload, verify=verify)
        if r.status_code == 201:
            self.token = r.json()['access_token']
            return True
        return False

    def login(self):
        logger.debug(self.email)
        url = urls.get('login')
        headers = {"Content-Type": "application/json"}
        payload = json.dumps({"email": self.email, "password": self.password})
        r = requests.post(url, headers=headers, data=payload, verify=verify)
        if r.status_code == 200:
            self.token = r.json()['access_token']
            return True
        return False

    def verify(self):
        logger.debug(self.email)
        url = urls.get('verify')
        headers = {"Content-Type": "application/json"}
        payload = json.dumps({"token": self.token})
        r = requests.post(url, headers=headers, data=payload, verify=verify)
        if r.status_code == 200:
            return True
        return False

    def logout(self):
        logger.debug(self.email)
        self.token = ''

    def list_users(self, query_string):
        logger.debug(self.email)
        url = urls.get('users')
        headers = {"Authorization": f"Bearer {self.token}"}
        payload = {"queryString": query_string}
        r = requests.get(url, headers=headers, params=payload, verify=verify)
        if r.status_code == 200:
            return True, r
        return False, r

    def __put_friendship__(self, addressee, status):
        url = urls.get('friendships')
        headers = {"Authorization": f"Bearer {self.token}", "Content-Type": "application/json"}
        payload = json.dumps({"addressee": addressee, "status": status})
        r = requests.put(url, headers=headers, data=payload, verify=verify)
        if r.status_code in (200, 201):
            return True, r
        return False, r

    def create_friendship(self, addressee):
        logger.debug(self.email)
        return self.__put_friendship__(addressee, 'Requested')

    def accept_friendship(self, addressee):
        logger.debug(self.email)
        return self.__put_friendship__(addressee, 'Accepted')

    def decline_friendship(self, addressee):
        logger.debug(self.email)
        return self.__put_friendship__(addressee, 'Declined')

    def delete_friendship(self, addressee):
        logger.debug(self.email)
        url = f"{urls['friendships']}"
        headers = {"Authorization": f"Bearer {self.token}"}
        payload = {"addressee": addressee}
        r = requests.delete(url, headers=headers, params=payload, verify=verify)
        if r.status_code == 200:
            return True
        return False

    def __get_friendships__(self, status):
        logger.debug(self.email)
        url = urls.get('friendships')
        headers = {"Authorization": f"Bearer {self.token}"}
        payload = {"status": status}
        r = requests.get(url, headers=headers, params=payload, verify=verify)
        if r.status_code == 200:
            return True, r
        return False, r

    def accepted_friendships(self):
        logger.debug(self.email)
        return self.__get_friendships__('Accepted')

    def requested_friendships(self):
        logger.debug(self.email)
        return self.__get_friendships__('Requested')

    def declined_friendships(self):
        logger.debug(self.email)
        return self.__get_friendships__('Declined')

    def __put_device__(self, device):
        url = urls.get('devices')
        headers = {"Authorization": f"Bearer {self.token}", "Content-Type": "application/json"}
        payload = json.dumps({"name": device.name, "uuid": device.uuid})
        return requests.put(url, headers=headers, data=payload, verify=verify)

    def create_device(self, device):
        logger.debug(self.email)
        r = self.__put_device__(device)
        if r.status_code == 201:
            device.owner = self.email
            self.devices.append(device)
            return True, r
        return False, r

    def update_device(self, device):
        logger.debug(self.email)
        r = self.__put_device__(device)
        if r.status_code == 200:
            index = None
            for i in range(len(self.devices)):
                if self.devices[i].uuid == device.uuid:
                    index = i
                    break
            self.devices[index] = device
            return True, r
        return False, r

    def delete_device(self, uuid):
        logger.debug(self.email)
        url = urls.get('devices')
        headers = {"Authorization": f"Bearer {self.token}"}
        payload = {"uuid": uuid}
        r = requests.delete(url, headers=headers, params=payload, verify=verify)
        if r.status_code == 200:
            return True, r
        return False, r

    def __get_devices__(self, addressee):
        logger.debug(self.email)
        url = urls.get('devices')
        headers = {"Authorization": f"Bearer {self.token}"}
        payload = {'addressee': addressee}
        r = requests.get(url, headers=headers, params=payload, verify=verify)
        if r.status_code == 200:
            return True, r
        return False, r

    def get_all_accessible_devices(self):
        logger.debug(self.email)
        return self.__get_devices__(addressee='')

    def get_own_devices(self):
        logger.debug(self.email)
        return self.__get_devices__(addressee=self.email)

    def get_friend_devices(self, addressee):
        logger.debug(self.email)
        return self.__get_devices__(addressee)
