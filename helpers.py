import logging
from auth import users

logger = logging.getLogger('test_logger')


def clean_friendships():
    logger.info('cleaning friendships')
    for user in users:
        user.login()
        f = [
            user.requested_friendships()[1].json()['users'],
            user.accepted_friendships()[1].json()['users'],
            user.declined_friendships()[1].json()['users'],
        ]
        all_friendships = []
        for friendships in f:
            if friendships:
                for friendship in friendships:
                    all_friendships.append(friendship)

        for friendship in all_friendships:
            user.delete_friendship(friendship['addressee'])


def clean_devices():
    logger.info('cleaning devices')
    for user in users:
        user.login()
        user.devices.clear()
        devices = user.get_own_devices()[1].json()['devices']
        if devices:
            for device in devices:
                user.delete_device(device['uuid'])
