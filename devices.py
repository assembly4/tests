import logging
from auth import users
from helpers import clean_devices, clean_friendships
from device_class import Device

logger = logging.getLogger('test_logger')


def device_tests():
    # delete all devices
    logger.info('delete all devices')
    clean_devices()

    # test authentication
    logger.info('test authentication')
    users[0].logout()
    r = users[0].create_device(Device('iphone 24'))
    if not r[0] and r[1].status_code == 401:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test validation
    logger.info('test validation')
    users[0].login()
    d1 = Device('')
    r1 = users[0].create_device(device=d1)[0]
    d2 = Device('random name')
    d2.uuid = ''
    r2 = users[0].create_device(device=d2)[0]
    if (not r1 or not r2) and not users[0].devices:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test creation
    logger.info('test creation')
    clean_devices()
    users[0].login()
    device = Device('iphone 0')
    r = users[0].create_device(device)
    if r[0] and users[0].devices:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test creation of the same device from different user
    logger.info('test creation of the same device from different user')
    users[1].login()
    r = users[1].create_device(device=device)
    if not r[0]:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test updating own device
    clean_devices()
    logger.info('test updating own device')
    users[0].login()
    users[0].create_device(Device('iphone 5'))
    d = users[0].devices[0]
    d.name = 'iphone 6'
    users[0].update_device(d)
    r = users[0].get_own_devices()
    if r[1].json()['devices'][0]['name'] == d.name:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test updating device of a different user
    logger.info('test updating device of a different user')
    users[1].login()
    r = users[1].update_device(users[0].devices[0])
    if not r[0]:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test listing own devices
    logger.info('test listing own devices')
    clean_devices()
    users[0].login()
    users[0].create_device(Device('ipad 7'))
    users[0].create_device(Device('ipad 5'))
    r = users[0].get_own_devices()
    ids_local = set(map(lambda x: x.uuid, users[0].devices))
    ids_remote = set(map(lambda x: x['uuid'], r[1].json()['devices']))
    if ids_local == ids_remote:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test listing friend's devices
    logger.info("test listing friend's devices")
    clean_devices()
    users[0].login()
    users[1].login()
    users[0].create_friendship(addressee=users[1].email)
    users[1].accept_friendship(addressee=users[0].email)
    users[1].create_device(Device('ipad 7'))
    r = users[0].get_friend_devices(addressee=users[1].email)
    ids_local = set(map(lambda x: x.uuid, users[1].devices))
    ids_remote = set(map(lambda x: x['uuid'], r[1].json()['devices']))
    if ids_local == ids_remote:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test listing all available devices
    logger.info("test listing all available devices")
    clean_devices()
    users[0].login()
    users[1].login()
    users[0].create_friendship(addressee=users[1].email)
    users[1].accept_friendship(addressee=users[0].email)
    users[0].create_device(Device('ipad 7'))
    users[1].create_device(Device('ipad 7'))
    r = users[0].get_all_accessible_devices()
    merged = users[0].devices + users[1].devices
    ids_local = set(map(lambda x: x.uuid, merged))
    ids_remote = set(map(lambda x: x['uuid'], r[1].json()['devices']))
    if ids_local == ids_remote:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test listing devices of a stranger
    logger.info("test listing devices of a stranger")
    clean_devices()
    users[0].login()
    r = users[0].get_friend_devices(addressee=users[5].email)
    clean_friendships()
    if not r[0]:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    return True
