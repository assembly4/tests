from uuid import uuid4


class Device:
    def __init__(self, name):
        self.name = name
        self.uuid = str(uuid4())
        self.owner = None

    def __repr__(self):
        return f'{self.uuid}'
