import logging
from auth import users
from helpers import clean_friendships

logger = logging.getLogger('test_logger')


def friendship_tests():
    # delete all friendships
    logger.info('delete all friendships')
    clean_friendships()

    # test authentication
    logger.info('test authentication')
    users[0].logout()
    r = users[0].create_friendship(addressee='')
    if not r[0] and r[1].status_code == 401:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test validation
    logger.info('test validation')
    users[0].login()
    r = users[0].create_friendship(addressee='not_an_email')
    if not r[0]:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test friendship creation with self
    logger.info('test friendship creation with self')
    users[0].login()
    r = users[0].create_friendship(addressee=users[0].email)
    if not r[0]:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test friendship creation with other existing user
    logger.info('test friendship creation with other existing user')
    users[0].login()
    users[0].delete_friendship(addressee=users[1].email)
    r = users[0].create_friendship(addressee=users[1].email)
    if r[0]:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test friendship listing of requested friendships from requester side
    logger.info('test friendship listing of requested friendships from requester side')
    users[0].login()
    r = users[0].requested_friendships()
    a = True if r[1].json()['users'][0]['addressee'] == users[1].email else False
    b = True if r[1].json()['users'][0]['requester'] == users[0].email else False
    if r[0] and a and b:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test friendship listing of requested friendships from addressee side
    logger.info('test friendship listing of requested friendships from addressee side')
    users[1].login()
    r = users[1].requested_friendships()
    a = True if r[1].json()['users'][0]['addressee'] == users[1].email else False
    b = True if r[1].json()['users'][0]['requester'] == users[0].email else False
    if r[0] and a and b:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test friendship deletion
    logger.info('test friendship deletion')
    users[0].login()
    users[0].create_friendship(addressee=users[1].email)
    users[0].delete_friendship(addressee=users[1].email)
    o = [
        users[0].requested_friendships(),
        users[0].accepted_friendships(),
        users[0].declined_friendships(),
    ]
    for r in o:
        if r[0] and not r[1].json()['users']:
            logger.info('ok')
        else:
            logger.error('nok')
            return False

    # test friendship Requested -> Accepted as requester
    logger.info('test friendship Requested -> Accepted as requester')
    clean_friendships()
    users[0].login()
    users[0].create_friendship(addressee=users[1].email)
    users[0].accept_friendship(addressee=users[1].email)
    r = users[0].accepted_friendships()
    if r[0] and not r[1].json()['users']:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test friendship Requested -> Accepted as addresse
    logger.info('test friendship Requested -> Accepted as addresse')
    clean_friendships()
    users[0].login()
    users[0].create_friendship(addressee=users[1].email)
    users[1].login()
    users[1].accept_friendship(addressee=users[0].email)
    r = users[0].accepted_friendships()
    if r[0] and r[1].json()['users'][0]['addressee'] == users[1].email:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test friendship None -> Accepted
    logger.info('test friendship None -> Accepted')
    users[0].login()
    users[0].delete_friendship(addressee=users[1].email)
    r = users[0].accept_friendship(addressee=users[1].email)
    if not r[0]:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test friendship creation with not existing user
    logger.info('test friendship creation with not existing user')
    users[0].login()
    r = users[0].create_friendship(addressee='random_email@domain.com')
    if not r[0]:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    return True
