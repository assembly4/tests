import logging
from auth import users

logger = logging.getLogger('test_logger')


def users_tests():
    # test authorization token
    logger.info(f'querying users with empty string without token')
    users[0].logout()
    if users[0].list_users(query_string='do')[1].status_code == 401:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test querying users with empty string
    logger.info(f'querying users with empty string')
    users[0].login()
    if users[0].list_users(query_string='')[1]:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test querying user (self) with exact pattern
    logger.info(f'querying user with exact (self) pattern')
    users[0].login()
    r = users[0].list_users(query_string=users[0].email)
    if r[1] and not r[1].json()['users']:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    # test querying user (other) with exact pattern
    logger.info(f'querying user with exact (other) pattern')
    users[0].login()
    r = users[0].list_users(query_string=users[1].email)
    if r[0] and len(r[1].json()['users']) == 1:
        logger.info('ok')
    else:
        logger.error('nok')
        return False

    return True
